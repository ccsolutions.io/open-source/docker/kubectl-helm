# Runbook for Docker Image with kubectl and helm

## Description

This Docker image includes `kubectl` and `helm`, the command-line tools for interacting with Kubernetes clusters. The base image used is Ubuntu.

## Build

1. To build this Docker image, navigate to the directory containing the Dockerfile and run:

   ```
   docker build --build-arg UBUNTU_VERSION=<your-version> -t <your-image-name> .
   ```

   Replace `<your-version>` with the Ubuntu version you want to use, and `<your-image-name>` with the name you want to give to your Docker image.

2. After the build completes, you can check that the image is available by running `docker images`.

## Run

1. To create and run a container from the image, you can use the `docker run` command:

   ```
   docker run -it <your-image-name> bash
   ```

   This will start a container and open an interactive bash shell inside it. You will have access to both `kubectl` and `helm` inside this container.

## Troubleshooting

1. **Issue**: Docker build fails
    - **Possible cause**: Docker daemon is not running.
    - **Solution**: Ensure Docker is installed and the Docker daemon is running.

2. **Issue**: Docker run fails with `Error: Unable to find image '<your-image-name>:latest' locally`
    - **Possible cause**: Image was not built or was not tagged correctly.
    - **Solution**: Ensure the image was built and tagged correctly.

3. **Issue**: `kubectl` or `helm` commands not found.
    - **Possible cause**: The Dockerfile was not built correctly, or there was an issue with the download during build.
    - **Solution**: Rebuild the Docker image and ensure there are no errors during the build process.

## Maintenance

This Dockerfile uses a specific version of Ubuntu, kubectl and helm. You may need to update these versions periodically to incorporate security updates and new features. Update the `UBUNTU_VERSION`, `kubectl` download URL, and `helm` download URL as needed, then rebuild the Docker image.

## Contacts

- Primary Maintainer: CCSolutions.io UG, devops@ccsolutions.io
