# Dockerfile Documentation

This Dockerfile creates a Docker image based on a specified version of Ubuntu, and includes the `kubectl` and `helm` command-line tools.

## Dockerfile Breakdown

### ARG UBUNTU_VERSION

This argument allows you to specify the version of Ubuntu that will be used as the base image. You can provide this at build-time with the `--build-arg` flag. For example:

```
docker build --build-arg UBUNTU_VERSION=20.04 -t my-image .
```

### FROM ubuntu:${UBUNTU_VERSION}

This line specifies the base image. The version of Ubuntu used will be determined by the `UBUNTU_VERSION` build argument.

### MAINTAINER CCSolutions.io UG <devops@ccsolutions.io>

The maintainer of the Dockerfile is specified here.

### RUN apt update ...

This line updates the package list and upgrades all the system's packages. It also installs `curl`, `tar`, and `gzip`.

### RUN curl -LO "https://dl.k8s.io/release/v1.27.3/bin/linux/amd64/kubectl" ...

This line downloads `kubectl`, makes it executable, and moves it to `/usr/local/bin/kubectl`.

### RUN curl -o helm-v3.12.2-linux-amd64.tar.gz ...

This line downloads `helm`, unpacks it, moves the `helm` binary to `/usr/local/bin/helm`, and cleans up the unpacked directory.

## Build Instructions

To build the Docker image, navigate to the directory containing the Dockerfile and run:

```
docker build --build-arg UBUNTU_VERSION=<your-version> -t <your-image-name> .
```

Replace `<your-version>` with the Ubuntu version you want to use, and `<your-image-name>` with the name you want to give to your Docker image.

After the build completes, you can check that the image is available by running `docker images`.

## Running a Container

To create and run a container from the image, you can use the `docker run` command:

```
docker run -it <your-image-name> bash
```

This will start a container and open an interactive bash shell inside it. You will have access to both `kubectl` and `helm` inside this container.

## Support

For support or any questions, please reach out to us at [devops@ccsolutions.io](mailto:devops@ccsolutions.io).

**Note:** Always be sure to follow best practices when configuring your Squid proxy server to ensure security and performance. If you need help with this, consider reaching out to a professional or consultant.

## Reporting Issues
If you encounter any issues while using this Dockerfile, please report them by creating a new issue in the [GitLab project](https://gitlab.com/ccsolutions.io/open-source/docker/kubectl-helm/-/issues).

## License

This project is licensed under the terms of the [MIT license](https://opensource.org/licenses/MIT).
