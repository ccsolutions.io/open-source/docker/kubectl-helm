ARG UBUNTU_VERSION
FROM ubuntu:${UBUNTU_VERSION}

MAINTAINER CCSolutions.io UG <devops@ccsolutions.io>

RUN apt update \
    && apt upgrade -y \
    && apt install curl tar gzip -y

RUN curl -LO "https://dl.k8s.io/release/v1.27.3/bin/linux/amd64/kubectl" \
    && chmod +x ./kubectl \
    && mv ./kubectl /usr/local/bin/kubectl

RUN curl -o helm-v3.12.2-linux-amd64.tar.gz https://get.helm.sh/helm-v3.12.2-linux-amd64.tar.gz \
    && tar -zxvf helm-v3.12.2-linux-amd64.tar.gz \
    && mv linux-amd64/helm /usr/local/bin/helm \
    && rm -rf linux-amd64



